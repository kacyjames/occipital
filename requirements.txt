Django==1.5.1
Fabric==1.6.0
South==0.7.6
Werkzeug==0.8.3
distribute==0.6.27
dj-database-url==0.2.1
django-debug-toolbar==0.9.4
django-extensions==1.1.0
djangorestframework==2.2.1
mimeparse==0.1.3
paramiko==1.10.0
psycopg2==2.4.6
pycrypto==2.6
python-dateutil==2.1
six==1.2.0
wsgiref==0.1.2
